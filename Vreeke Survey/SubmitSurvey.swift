//
//  SubmitSurvey.swift
//  Vreeke Survey
//
//  Created by Alokin on 30/01/19.
//  Copyright © 2019 Alokin. All rights reserved.
//

import UIKit

class SubmitSurvey: BaseViewController, DownloadDelegate {
    @IBOutlet weak var progressBar: UIProgressView!
    var progressBarTimer: Timer!
    // label and 2 buttons
    @IBOutlet weak var startSurvey: UIButton!
    @IBOutlet weak var manageSurveys: UIButton!
    @IBOutlet weak var surveyStatus: UILabel!
    @IBOutlet weak var surveyCount: UILabel!
    @IBOutlet weak var versionTag: UILabel!
    @IBOutlet weak var separatorView: UIView!

    let SURVEY_SUCCESS = "Your survey has been sent successfully!"
    let SURVEY_FAILURE = "Your survey was not submitted"
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .submitResponse, object: nil)
        NotificationCenter.default.removeObserver(self, name: .submitResponseFailed, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(submitSuccess), name: .submitResponse, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showSurveyButtonsWithWarning), name: .submitResponseFailed, object: nil)
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        logdata(#file, #function)
//        NotificationCenter.default.addObserver(self, selector: #selector(submitSuccess), name: .submitResponse, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(showSurveyButtonsWithWarning), name: .submitResponseFailed, object: nil)
        progressBarTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { timer in
            if self.progressBar.progress == 1.0 {
                timer.invalidate()
                self.showSurveyButtonsWithWarning()
            } else {
                self.progressBar.setProgress(self.progressBar.progress + 0.005, animated: true)
            }
        })

        /*
         upload images from survery response object
         update response object with image ids
        */
        versionTag.text = VERSION_TAG
        startSurvey.setRoundedCorners()
        manageSurveys.setRoundedCorners()

        if connectedToNetwork() {
            logdata(appData.surveyResponse)
            let psq = ProcessSurveyQuestions(questions: (appData.surveyResponse?.survey_response)!)
            psq.delegate = self
            psq.processQuestion()
        } else {
            // show alert
            self.showSurveyButtonsWithWarning()
        }
    }

    @objc func showSurveyButtonsWithWarning() {
        DispatchQueue.main.async {
            self.progressBar.setProgress(1.0, animated: true)
            appData.clearDBResponse()
            self.surveyStatus.text = self.SURVEY_FAILURE
            let alert = UIAlertController(title: "Warning", message: "The survey could not be submitted. Please retry when you are online", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            self.present(alert, animated: true, completion: {
                self.showSurveyButtons()
            })
        }
    }

    func showSurveyButtons() {
        startSurvey.isHidden = false
        manageSurveys.isHidden = false
        let pendingSurveyCount = getPendingSurveyCount()
        if pendingSurveyCount > 0 {
            surveyCount.text = getPendingSurveyText(pendingSurveyCount)
        }
        surveyCount.isHidden = false
        separatorView.isHidden = false
    }

    @objc func submitSuccess() {
        logdata(#file, #function, appData.dbResponse)
        progressBarTimer.invalidate()
//        database.updateData(of: "Response", with: "date=%@", arguments: [appData.dbResponse?.date], data: ["submitted": true])
        if database.deleteData(from: "Response", with: "date=%@", arguments: [appData.dbResponse!.date]) {
//            let urlList = appData.dbResponse?.response.getImageURLs()
//            for urls in urlList ?? [] {
//                for url in urls {
//                    if url.contains("userUploads") {
//                        deleteFileAtPath(path: url)
//                    }
//                }
//            }
            appData.deleteUserUploadedImages()
            DispatchQueue.main.async {
                self.progressBar.setProgress(1.0, animated: true)
                self.surveyStatus.text = self.SURVEY_SUCCESS
                self.showSurveyButtons()
                appData.clearDBResponse()
            }
        } else {
            // warning??
        }
    }

    func didComplete(_ data: Any?) {
        logdata(#file, #function, data)
        logdata("process questions complteted", appData.surveyResponse)
        DispatchQueue.main.async {
            self.progressBar.setProgress(0.5, animated: true)
        }
        appData.surveyResponse?.survey_response = data as! [SurveyQuestion]
        do {
            let jsonData = try encoder.encode(appData.surveyResponse)
            request.submitResponse(data: jsonData)
        } catch let err {
            logdata(err)
        }
    }
}
