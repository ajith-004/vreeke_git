//
//  SurveyQuestionImagesView.swift
//  Vreeke Survey
//
//  Created by Alokin on 28/01/19.
//  Copyright © 2019 Alokin. All rights reserved.
//

import Foundation
import UIKit

class SurveyQuestionImageWrapperView: UIView, ImageRequiredDelegate {
    var hasImage: [Bool] = [Bool]()

    override init(frame: CGRect) {
        super.init(frame: frame)
//        customInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        customInit()
    }
    
//    func customInit() {
//        NotificationCenter.default.addObserver(self, selector: #selector(imageUploaded), name: .imageUploaded, object: nil)
//    }
//
//    @objc func imageUploaded() {
//        logdata(#file, #function)
//    }
    
    func selectedImageRequiredOption() {
        logdata(#file, #function, self.subviews)
        for subview in self.subviews as! [ImageSection] {
            subview.selectedImageRequiredOption()
        }
    }
    
    func unselectedImageRequiredOption() {
        logdata(#file, #function, self.subviews)
        for subview in self.subviews as! [ImageSection] {
            subview.unselectedImageRequiredOption()
        }
    }

    func renderImageSection(_ imageData: [ImageData]) {
        var yPos: CGFloat = 0
        hasImage = Array(repeating: false, count: imageData.count)
        var imagePreviewHeight: CGFloat
        if UIScreen.main.bounds.width < 768 {
            imagePreviewHeight = 140
        } else {
            imagePreviewHeight = 180
        }
        for (idx, data) in imageData.enumerated() {
            let wrapperView = ImageSection(frame: CGRect(x: 0, y: yPos, width: self.frame.width, height: imagePreviewHeight))
//            wrapperView.initWithData(data, index: idx)
            self.addSubview(wrapperView)
            wrapperView.initWithData(data, index: idx)
            yPos = wrapperView.frame.maxY
        }
        self.frame = CGRect(x: self.frame.minX, y: self.frame.minY, width: self.frame.width, height: yPos)
    }

    func getImageData() -> [ImageData] {
        let imageSections = self.subviews.filter({ $0 is ImageSection })
        let imageData = imageSections.map({(view: UIView) -> ImageData in
            let imageSection = view as! ImageSection
            return imageSection.requestImageData()
        })
        return imageData
    }
    
    func validateImages() -> Bool {
        logdata(#function, hasImage)
        if hasImage.count > 0 {
            if hasImage.contains(true) {
                logdata(#function, "image section validation pass")
                return true
            }
            logdata(#function, "image section validation failed")
            return false
        }
        logdata(#function, "no image section, return true")
        return true
    }

}
