//
//  ViewController.swift
//  Vreeke Survey
//
//  Created by Alokin on 15/01/19.
//  Copyright © 2019 Alokin. All rights reserved.
//

import UIKit
import CoreData

class ViewController: BaseViewController, UITextFieldDelegate {
    
    @IBOutlet weak var validationError: UILabel!
    @IBOutlet weak var dealerID: UITextField!
    @IBOutlet weak var repID: UITextField!
    @IBOutlet weak var startSurvey: UIButton!
    @IBOutlet weak var manageSurvey: UIButton!
    @IBOutlet weak var surveyCount: UILabel!
    @IBOutlet weak var versionTag: UILabel!
    @IBOutlet weak var loaderView: UIView!
//    @IBOutlet weak var scrollView: UIScrollView!
    var downloadDetailListFailed: Bool!
    
    @IBAction func doLogin(_ sender: Any) {
//        let offlineTest = true
        if downloadDetailListFailed {
            self.loaderView.isHidden = false
            request.getDealersList()
        } else {
            let dealerIdValue: String = dealerID.text ?? ""
            let repIdValue: String = repID.text ?? ""
            logdata(#function, appData.dealerList, appData.dealerList?.result)
            let matchedInfo: [DealerData] = appData.dealerList?.result.filter{ $0.dealerID == dealerIdValue} ?? []
            let validationMessage = validateCredentials(dealerID: dealerIdValue, repID: repIdValue)
            if validationMessage.count > 0 {
                loginFailed(validationMessage)
            } else {
                if matchedInfo.count > 0 {
                    appData.dealerData = matchedInfo[0]
                    appData.dealerData?.repID = repIdValue
                    loginSuccess()
                } else {
                    loginFailed()
                }
            }
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case dealerID:
            repID.becomeFirstResponder()
        default:
            repID.resignFirstResponder()
            doLogin(startSurvey)
        }
        return true
    }
    
    /// Validate credentials and returns validation error if any
    func validateCredentials(dealerID: String, repID: String) -> String {
        let dealer = dealerID.count > 0 ? "" : "Dealer #"
        let rep = repID.count > 0 ? "" : "Zone & District"
        if dealer.count == 0 && rep.count == 0 {
            return ""
        } else {
            return "\(dealer)\(dealer.count > 0 && rep.count > 0 ? " and " : "")\(rep) is required"
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .loginSuccess, object: nil)
        NotificationCenter.default.removeObserver(self, name: .loginFailed, object: nil)
        NotificationCenter.default.removeObserver(self, name: .dealerListDownloaded, object: nil)
        NotificationCenter.default.removeObserver(self, name: .dealerListDownloadFailed, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(loginSuccess), name: .loginSuccess, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loginFailed), name: .loginFailed, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dealerListDownloaded), name: .dealerListDownloaded, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dealerListDownloadFailed), name: .dealerListDownloadFailed, object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appData.clearDBResponse()
        downloadDetailListFailed = false
        // Register notifications
//        NotificationCenter.default.addObserver(self, selector: #selector(loginSuccess), name: .loginSuccess, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(loginFailed), name: .loginFailed, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(dealerListDownloaded), name: .dealerListDownloaded, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(dealerListDownloadFailed), name: .dealerListDownloadFailed, object: nil)
        //TODO: implement scrollview - handle keyboard events
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown), name: UIResponder.keyboardWillShowNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        // UI setup
        let dealerIDPadding = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.dealerID.frame.height))
        dealerID.leftView = dealerIDPadding
        dealerID.leftViewMode = .always
        dealerID.attributedPlaceholder = NSAttributedString(string: "Dealer #", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        let repIDPadding = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.repID.frame.height))
        repID.leftView = repIDPadding
        repID.leftViewMode = .always
        repID.attributedPlaceholder = NSAttributedString(string: "Zone & District", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        dealerID.delegate = self
        repID.delegate = self
        startSurvey.setRoundedCorners()
        manageSurvey.setRoundedCorners()
        versionTag.text = VERSION_TAG
        let pendingSurveyCount = getPendingSurveyCount()
        if pendingSurveyCount > 0 {
            surveyCount.isHidden = false
            surveyCount.text = getPendingSurveyText(pendingSurveyCount)
        }


        if connectedToNetwork() {
//            logdata(#file, #function, "has net")
            request.getDealersList()
        } else {
            dealerListDownloadFailed()
        }
    }
    
//    @objc func keyboardWasShown(_ notification: NSNotification) {
//        logdata(#function, notification)
//        let kbSize = (notification.userInfo!["UIKeyboardFrameEndUserInfoKey"] as! CGRect).size
//
//        let contentInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: kbSize.height, right: 0)
//        scrollView.contentInset = contentInsets
//        scrollView.scrollIndicatorInsets = contentInsets
//    }
//
//    @objc func keyboardWillBeHidden(_ notification: NSNotification) {
//        logdata(#function, notification)
//        let contentInsets = UIEdgeInsets.zero
//        scrollView.contentInset = contentInsets
//        scrollView.scrollIndicatorInsets = contentInsets
//    }

    @objc func dealerListDownloadFailed() {
        DispatchQueue.main.async {
            if !self.dealerListInDB() {
                self.downloadDetailListFailed = true
                let alert = UIAlertController(title: "Warning", message: "Dealer data not available. Please retry when you are online", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            self.hideLoaderView()
        }
    }

    func hideLoaderView() {
        DispatchQueue.main.async {
            self.loaderView.isHidden = true
        }
    }

    @objc func dealerListDownloaded() {
        // fetch data from db
//        logdata(#function)
        if dealerListInDB() {
            if self.downloadDetailListFailed {
                logdata("dealerListDownloaded", "DO LOGIn")
                self.downloadDetailListFailed = false
                self.doLogin(self)
            }
            self.hideLoaderView()
        }
    }

    func dealerListInDB() -> Bool {
        var dealerList = database.readData(from: "Dealers", with: nil, arguments: nil) as! [NSManagedObject]
        if dealerList.count > 0 {
            // has data
            let data = dealerList[0]
            let list = data.value(forKey: "dealerList") as! String
            appData.setDealerList(try! decoder.decode(DealerList.self, from: stringToData(list)))
            return true
        }
        return false
    }
    
    @objc func loginSuccess() {
//        logdata("Received login success notification")
        DispatchQueue.main.async {
            self.validationError.isHidden = true
            self.performSegue(withIdentifier: "fetchSurvey", sender: self)
        }
        
    }
    
    @objc func loginFailed(_ message: String = "Invalid Dealer ID") {
//        logdata("Received login failed notification")
        DispatchQueue.main.async {
            self.validationError.text = message
            self.validationError.isHidden = false
        }
    }

}



