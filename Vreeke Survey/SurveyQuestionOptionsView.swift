//
//  SurveyQuestionOptionsView.swift
//  Vreeke Survey
//
//  Created by Alokin on 25/01/19.
//  Copyright © 2019 Alokin. All rights reserved.
//

import Foundation
import UIKit

class SurveyQuestionOptionsView: UIView {
    var optionSelected: Bool = false
    var delegate: ImageRequiredDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func renderOptions(_ options: [OptionData]) {
        var buttonMaxX: CGFloat = 0
        var buttonHeight, buttonWidth: CGFloat
        buttonHeight = 50
        buttonWidth = 70
//        if UIScreen.main.bounds.width < 768 {
//            buttonHeight = 38
//            buttonWidth = 67
//        } else {
//            buttonHeight = 50
//            buttonWidth = 70
//        }
        var yPos: CGFloat = 0
        var buttonRow: CGFloat = 1
        var lastView: UIView = UIView(frame: CGRect(x: buttonMaxX, y: yPos, width: 0, height: 0))
        for option in options {
            let optionButton = OptionButton(type: .custom)
            optionButton.frame = CGRect(x: buttonMaxX, y: yPos, width: buttonWidth, height: buttonHeight)
            optionButton.setTitle(option.option.uppercased(), for: .normal)
            optionButton.titleLabel?.font = UIFont(name: "Montserrat-Bold", size: 18)!
            optionButton.imageRequired = option.imageRequired ?? false
            optionButton.sizeToFit() // updates the views height and width based on content
            if option.checked ?? false {
                optionButton.isSelected = true
                optionButton.backgroundColor = buttonRed
                optionSelected = true
                postNotification(.selectOption, userInfo: nil)
//                optionButton.imageRequired ? self.delegate?.selectedImageRequiredOption() : self.delegate?.unselectedImageRequiredOption()
                fireOptionSelectedCall(for: optionButton)
            } else {
                optionButton.isSelected = false
                optionButton.backgroundColor = buttonGrey
            }
            // For rounded corners
//            optionButton.setRoundedCorners()
            optionButton.layer.cornerRadius = 25
            optionButton.layer.borderWidth = 1
            optionButton.layer.borderColor = UIColor.clear.cgColor

            optionButton.optionData = option
            optionButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
            // Set minimum height and width so that the button looks like one!
            let width = addSpacing(optionButton.frame.width > buttonWidth ? optionButton.frame.width : buttonWidth, offset: 20)
            if buttonMaxX + width > self.bounds.width {
                yPos = addSpacing(buttonHeight * buttonRow, offset: 10 * buttonRow)
                buttonMaxX = 0
                buttonRow += 1
            }
            optionButton.frame = CGRect(x: buttonMaxX, y: yPos, width: width, height: buttonHeight)
            buttonMaxX = addSpacing(optionButton.frame.maxX)
            lastView = optionButton
            self.addSubview(optionButton)
        }
        self.frame = CGRect(x: self.frame.minX, y: self.frame.minY, width: self.frame.width, height: lastView.frame.maxY)
    }
    
    func fireOptionSelectedCall(for button: OptionButton) {
        button.imageRequired ? self.delegate?.selectedImageRequiredOption() : self.delegate?.unselectedImageRequiredOption()
    }

    @objc func buttonPressed(sender: OptionButton) {
        for button in self.subviews as! [OptionButton] {
            button.isSelected = false
            button.backgroundColor = buttonGrey
            button.optionData!.checked = false
        }
        sender.isSelected = true
        sender.backgroundColor = buttonRed
        sender.optionData!.checked = true
        if !optionSelected {
            postNotification(.selectOption, userInfo: nil)
        }
//        sender.imageRequired ? self.delegate?.selectedImageRequiredOption() : self.delegate?.unselectedImageRequiredOption()
        fireOptionSelectedCall(for: sender)
    }

    func getOptionData() -> [OptionData] {
        var data: [OptionData] = []
        for subview in self.subviews as! [OptionButton] {
            data.append(subview.optionData!)
        }
        return data
    }
}


protocol ImageRequiredDelegate {
    func selectedImageRequiredOption()
    func unselectedImageRequiredOption()
}
