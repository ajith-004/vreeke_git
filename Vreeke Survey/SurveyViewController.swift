//
//  SurveyViewController.swift
//  Vreeke Survey
//
//  Created by Alokin on 16/01/19.
//  Copyright © 2019 Alokin. All rights reserved.
//

import UIKit
import CoreData

class SurveyViewController: BaseViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    @IBOutlet weak var dealerID: UILabel!
    @IBOutlet weak var displayID: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var popupWrapper: UIView!
    @IBOutlet weak var popupClose: UIButton!
    @IBOutlet weak var popupImage: UIImageView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var saveLoader: UIView!
    @IBOutlet weak var headerLabel: UILabel!

    var submitSurvey: UIButton!
    var resetSurvey: UIButton!
    var isCompletedSurvey: Bool = false
    var userResponse: [SurveyQuestion]!
    var imageValidationStatus: [Bool]!
    var surveyQuestionViews: [UIView]!

    @IBAction func hidePopup(_ sender: Any) {
        popupWrapper.isHidden = true
    }

    var imagePicker: UIImagePickerController!
    var childImageSection: ImageSection?
    var questionCount: Int?
    var selectedOptionCount: Int = 0

    @IBAction func saveAndReset(_ sender: Any) {
//        self.resetQuestions()
//        self.renderQuestions()
        DispatchQueue.main.async {
            self.saveLoader.isHidden = false
            Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: {timer in
                if self.generateResponse() {
                    self.saveResponseToDb()
                    self.performSegue(withIdentifier: "saveAndRestart", sender: self)
                }
            })
//            self.saveResponseToDb()
//            self.performSegue(withIdentifier: "saveAndRestart", sender: self)
        }
    }

    @IBAction func submitResponse(_ sender: Any) {
//        DispatchQueue.main.async {
//            self.saveLoader.isHidden = false
//        }

//        appData.setDBResponse(saveResponseToDb(completed: true))
        // show survey submission screen
        DispatchQueue.main.async {
            self.saveLoader.isHidden = false
            Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: {timer in
                if self.generateResponse(completed: true) {
                    appData.setDBResponse(self.saveResponseToDb(completed: true))
                    self.performSegue(withIdentifier: "submitSurvey", sender: self)
                } else {
                    logdata("error failed validation")
                    // Show alert
                    self.saveLoader.isHidden = true
                    let alert = UIAlertController(title: "Attention", message: "Some of the required images are missing. Please upload them to continue", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {_ in
                        for (idx, validationPassed) in self.imageValidationStatus.enumerated() {
                            self.updateErrorMessageStatus(view: self.surveyQuestionViews[idx], hideErrorMessage: validationPassed)
                        }
//                        for view in errorViews {
//                            let sqView = view as! SurveyQuestionView
//                            let errorWrapper = sqView.getErrorMessageWrapper()
//                            if errorWrapper.count > 0 {
//                                let stackview = errorWrapper[0] as! UIStackView
//                                stackview.arrangedSubviews[1].isHidden = false
//                            }
//                        }
                        let firstErrorIndex = self.imageValidationStatus.firstIndex(of: false)
                        logdata("testing... error index", self.imageValidationStatus, firstErrorIndex)
                        if firstErrorIndex != nil {
                            let invalidQuestionFrame = self.surveyQuestionViews[firstErrorIndex!].frame
                            self.scrollView.scrollRectToVisible(invalidQuestionFrame, animated: true)
                        }
                        
                    }))
                    self.present(alert, animated: true, completion: {
                        logdata("completed")
                    })
                }
            })
        }
    }
    
    func updateErrorMessageStatus(view: UIView, hideErrorMessage: Bool) {
        let sqView = view as! SurveyQuestionView
        let errorWrapper = sqView.getErrorMessageWrapper()
        if errorWrapper.count > 0 {
//            let stackview = errorWrapper[0] as! UIStackView
//            stackview.arrangedSubviews[1].isHidden = hideErrorMessage
            errorWrapper[0].isHidden = hideErrorMessage
        }
    }

    func resetQuestions() {
        let result = database.readData(from: "Question", with: nil, arguments: nil)
        if result.count > 0 {
            let data = result[0] as! NSManagedObject
            let question = data.value(forKey: "question") as! String
            appData.setSurveyQuestions(using: stringToData(question))
        }
        appData.clearDBResponse(clearDealerInfo: false)
    }
    
    func generateResponse(completed: Bool = false) -> Bool {
//        let sqviews = scrollView.subviews.filter({ $0 is SurveyQuestionView })
        surveyQuestionViews = scrollView.subviews.filter({ $0 is SurveyQuestionView })
        userResponse = surveyQuestionViews.map({(view: UIView) -> SurveyQuestion in
            let sqView = view as! SurveyQuestionView
            return sqView.getResponseData()
        })
        logdata(#function, userResponse)
        imageValidationStatus = userResponse.map({ $0.imageValidation ?? false })
        if completed && imageValidationStatus.contains(false) {
            return false
        }
        return true
    }

    func saveResponseToDb(completed: Bool =  false) -> DBResponse {
        var timestamp: Date?

//        let sqviews = scrollView.subviews.filter({ $0 is SurveyQuestionView })
//        let userResponse = sqviews.map({(view: UIView) -> SurveyQuestion in
//            let sqView = view as! SurveyQuestionView
//            return sqView.getResponseData()
//        })
//        logdata(userResponse)
        
        let response = SurveyResponse(dealer: (appData.dealerData?.dealerID)!, rep: (appData.dealerData?.repID)!, response: userResponse, displayID: appData.dealerData!.displayID)
//        logdata("response", response)
        let jsonData = try! encoder.encode(response)

        if appData.dbResponse != nil {
            timestamp = appData.dbResponse?.date
            logdata("delete", database.deleteData(from: "Response", with: "date=%@", arguments: [timestamp!]))
        }

        timestamp = Date()
        logdata("save", database.saveData(to: "Response", data: [
            "completed": completed,
            "submitted": false,
            "date": timestamp!,
            "response": dataToString(jsonData)
        ]))

        return DBResponse(date: timestamp!, response: response, completed: completed, submitted: false)
    }

    @objc func captureImage(_ notification: NSNotification) {
        let data: [String: ImageSection] = notification.userInfo as! [String: ImageSection]
        childImageSection = data["imageSection"]
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        present(imagePicker, animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .captureImage, object: nil)
        NotificationCenter.default.removeObserver(self, name: .selectOption, object: nil)
        NotificationCenter.default.removeObserver(self, name: .showPopup, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(captureImage), name: .captureImage, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(optionSelected), name: .selectOption, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showPopup), name: .showPopup, object: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        NotificationCenter.default.addObserver(self, selector: #selector(captureImage), name: .captureImage, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(optionSelected), name: .selectOption, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(showPopup), name: .showPopup, object: nil)
//        self.dealerID.text = AppData.sharedInstance.dealerId
//        self.displayID.text = AppData.sharedInstance.displayId
        questionCount = appData.surveyQuestions?.count
        // UI updates
//        dealerID.text = getDealerID()
//        displayID.text = getDisplayID()
        dealerID.attributedText = getFormattedString(prefix: "Dealer #: ", value: (appData.dealerData?.dealerID)!)
        displayID.attributedText = getFormattedString(prefix: "Display used: ", value: (appData.dealerData?.displayID)!)
        headerView.setShadow()

        let attrString1 = NSMutableAttributedString(string: "Please answer the following questions ", attributes:[NSAttributedString.Key.font: UIFont(name: "Montserrat-Bold", size: 16)])
        let attrString2 = NSAttributedString(string: "and take photos where indicated.")
        attrString1.append(attrString2)
        headerLabel.attributedText = attrString1
        headerLabel.sizeToFit()

//        for family in UIFont.familyNames.sorted() {
//            let names = UIFont.fontNames(forFamilyName: family)
//            logdata("Family: \(family) Font names: \(names)")
//        }

        renderQuestions()
    }
    
    func getFormattedString(prefix: String, value: String) -> NSMutableAttributedString {
        let prefix = NSMutableAttributedString(string: prefix)
        let value = NSAttributedString(string: value, attributes: [NSAttributedString.Key.font: UIFont(name: "Montserrat-Bold", size: 16)])
        prefix.append(value)
        return prefix
    }

    func getDealerID() -> String {
        return "Dealer number: \((appData.dealerData?.dealerID)!)"
    }

    func getDisplayID() -> String {
        return "Display used: \((appData.dealerData?.displayID)!)"
    }

    @objc func showPopup(_ notification: NSNotification) {
        let data: [String: UIImageView] = notification.userInfo as! [String: UIImageView]
        popupImage.image = data["imageView"]?.image
        popupWrapper.isHidden = false
    }

    @objc func optionSelected() {
        selectedOptionCount += 1
        if selectedOptionCount == questionCount {
            submitButton.isEnabled = true
            if submitSurvey != nil {
                submitSurvey.isEnabled = true
                submitSurvey.backgroundColor = buttonRed
            } else {
                isCompletedSurvey = true
            }
        }
        // TODO: add alert?
    }

    func renderQuestions() {
        clearSubviews()
        var subViewY: CGFloat = 0
        logdata(#function, appData.dealerData)
        let questionCount = appData.surveyQuestions?.count
        for (index, sq) in appData.surveyQuestions!.enumerated() {
            logdata(sq)
            let sqView: SurveyQuestionView = SurveyQuestionView(frame: CGRect(x: 0, y: subViewY, width: view.frame.width, height: 90))
            sqView.updateWithData(sq, index: index)
            if index + 1 == questionCount {
                sqView.setShadow()
            }
            scrollView.addSubview(sqView)
            subViewY = sqView.frame.maxY + 5
        }
//        let shadowView: UIView = UIView(frame: CGRect(x: 0, y: subViewY, width: view.frame.width, height: 3))
//        shadowView.isOpaque = true
//        shadowView.setShadow(shadowOffset: CGSize(width: -5, height: 8))
//        scrollView.addSubview(shadowView)
        var resetButtonWidth, submitButtonWidth, buttonPadding: CGFloat
        if UIScreen.main.bounds.width < 768 {
            resetButtonWidth = 170
            submitButtonWidth = 140
            buttonPadding = 20
        } else {
            resetButtonWidth = 200
            submitButtonWidth = 150
            buttonPadding = 30
        }
        // Add restart and submit buttons
        resetSurvey = UIButton(type: .custom)
        resetSurvey.frame = CGRect(x: buttonPadding, y: addSpacing(subViewY, offset: 20), width: resetButtonWidth, height: 50)
        resetSurvey.setTitle("SAVE & RESTART", for: .normal)
        resetSurvey.titleLabel?.font = UIFont(name: "Montserrat-Bold", size: 16)!
        resetSurvey.setTitleColor(UIColor.black, for: .normal)
        resetSurvey.setRoundedCorners()
        resetSurvey.layer.borderColor = UIColor.black.cgColor
        resetSurvey.addTarget(self, action: #selector(saveAndReset), for: .touchUpInside)
        scrollView.addSubview(resetSurvey)

        submitSurvey = UIButton(type: .custom)
        submitSurvey.frame = CGRect(x: self.view.frame.width - addSpacing(submitButtonWidth, offset: buttonPadding), y: addSpacing(subViewY, offset: 20), width: submitButtonWidth, height: 50)
        submitSurvey.setTitle("SUBMIT", for: .normal)
        submitSurvey.setTitleColor(.white, for: .normal)
        submitSurvey.titleLabel?.font = UIFont(name: "Montserrat-Bold", size: 16)!
        submitSurvey.setRoundedCorners()
        submitSurvey.backgroundColor = buttonGrey
        submitSurvey.isEnabled = false
        submitSurvey.addTarget(self, action: #selector(submitResponse), for: .touchUpInside)
        scrollView.addSubview(submitSurvey)
        if isCompletedSurvey {
            submitSurvey.isEnabled = true
            submitSurvey.backgroundColor = buttonRed
        }
        // Update scrollview content size
        scrollView.contentSize = CGSize(width: scrollView.frame.width, height: addSpacing(submitSurvey.frame.maxY, offset: 30))
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        guard let image = info[.originalImage] as? UIImage else {
            return
        }
        childImageSection!.postImageCapture(image)
    }

    func clearSubviews() {
        scrollView.subviews.forEach({ $0.removeFromSuperview() })
    }
}
