//
//  Extensions.swift
//  Vreeke Survey
//
//  Created by Alokin on 24/01/19.
//  Copyright © 2019 Alokin. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    func image(_ size: CGSize = CGSize(width: 1, height: 1)) -> UIImage {
        return UIGraphicsImageRenderer(size: size).image { rendererContext in
            self.setFill()
            rendererContext.fill(CGRect(origin: .zero, size: size))
        }
    }
}

extension Notification.Name {
    static let loginFailed = Notification.Name("loginFailed")
    static let loginSuccess = Notification.Name("loginSuccess")
    static let getSurveyDataSuccess = Notification.Name("getSurveyDataSuccess")
    static let captureImage = Notification.Name("captureImage")
    static let downloadImageComplete = Notification.Name("downloadImageComplete")
    static let submitResponse = Notification.Name("submitResponse")
    static let submitResponseFailed = Notification.Name("submitResponseFailed")
    static let submitAllResponse = Notification.Name("submitAllResponse")
    static let submitAllResponseFailed = Notification.Name("submitAllResponseFailed")
//    static let continueSurvey = Notification.Name("continueSurvey")
//    static let manageToSubmit = Notification.Name("manageToSubmit")
    static let selectOption = Notification.Name("selectOption")
    static let dealerListDownloaded = Notification.Name("dealerListDownloaded")
    static let dealerListDownloadFailed = Notification.Name("dealerListDownloadFailed")
    static let showPopup = Notification.Name("showPopup")
//    static let imageUploaded = Notification.Name("imageUploaded")
}

extension String {
    func indices(of occurrence: String) -> [Int] {
        var indices = [Int]()
        var position = startIndex
        while let range = range(of: occurrence, range: position..<endIndex) {
            let i = distance(from: startIndex,
                             to: range.lowerBound)
            indices.append(i)
            let offset = occurrence.distance(from: occurrence.startIndex,
                                             to: occurrence.endIndex) - 1
            guard let after = index(range.lowerBound,
                                    offsetBy: offset,
                                    limitedBy: endIndex) else {
                                        break
            }
            position = index(after: after)
        }
        return indices
    }
}

class OptionButton: UIButton {
    var optionData: OptionData?
    var imageRequired: Bool = false
    var imageUploaded: Bool = false
}

extension UIButton {
    func setRoundedCorners() {
        self.layer.cornerRadius = self.frame.height / 2
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.clear.cgColor
    }
}

extension UIView {
    /**
     Sets the views shadow. By default this function sets bottom shadow for any given view.
     - Parameter shadowOffset: The offset (in points) of the layer’s shadow
     - Parameter shadowOpacity: The opacity of layers shadow
     - Parameter shadowRadius: The blur radius (in points) used to render the layer’s shadow
     - Parameter shadowColor: The color of the layer's shadow
    */
    func setShadow(shadowOffset: CGSize = CGSize(width: 0, height: 3), shadowOpacity: Float = 0.6, shadowRadius: CGFloat = 3.0, shadowColor: UIColor = UIColor.lightGray) {
        self.layer.shadowOffset = shadowOffset
        self.layer.shadowOpacity = shadowOpacity
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowColor = shadowColor.cgColor
    }
}

/**
 Base view controller for this project, with hidden status bar
 */
class BaseViewController: UIViewController {
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
}


class ErrorLabel: UILabel {
    
}
