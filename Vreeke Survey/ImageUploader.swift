//
//  ImageUploader.swift
//  Vreeke Survey
//
//  Created by Alokin on 30/01/19.
//  Copyright © 2019 Alokin. All rights reserved.
//

import Foundation
import Alamofire


protocol DownloadDelegate {
    func didComplete(_ data: Any?)
}

class ImageUploader {
    var imageData: ImageData
    let uploadURL = request.generateURL("upload-image", appendEndpoint: true)
    var delegate: DownloadDelegate?
    let httpHeaders: HTTPHeaders = ["api-token": API_TOKEN]

    init(imageData: ImageData) {
        self.imageData = imageData
    }

    func upload() {
        logdata("Uploading....")
        if imageData.url.contains("userUploads") {
            logdata("new image")
            Alamofire.upload(
                multipartFormData:{ multipartFormData in
                    multipartFormData.append(getFileSavePath(self.imageData.url), withName: "image")
                },
                to: uploadURL,
                headers: httpHeaders,
                encodingCompletion: { encodingResult in
                    switch encodingResult {
                        case .success(let upload, _, _):
                            upload.responseJSON { response in
                                logdata(#file, #function, response)
                                do {
                                    let data = try decoder.decode(ImageUploadResponse.self, from: response.data!)
                                    self.imageData.url = String(data.id)
                                    logdata("upload Success", self.imageData.url)
                                } catch let err {
                                    logdata(err)
                                    self.imageData.url = "error"
                                }
                                self.delegate?.didComplete(self.imageData)
                            }
                        case .failure(let encodingError):
                            print(encodingError)
                    }
                }
            )
        } else {
            logdata("sample image")
            self.delegate?.didComplete(self.imageData)
        }
    }
}

//            ).responseJSON { response in
//                logdata("response", response)
//                do {
//                    let data = try decoder.decode(ImageUploadResponse.self, from: response.data!)
//                    self.imageData.url = String(data.id)
//                } catch let err {
//                    logdata(err)
//                    self.imageData.url = "error"
//                }
//                self.delegate?.didComplete(self.imageData)
//            }
//        } else {
//            logdata("sample image")
//            self.delegate?.didComplete(self.imageData)
//        }


class ProcessImages: DownloadDelegate {
    var surveyQuestion: SurveyQuestion
    var imageCount: Int
    var uploadCount: Int
    var uploader: ImageUploader?
    var delegate: DownloadDelegate?

    init(of surveyQuestion: SurveyQuestion) {
        self.surveyQuestion = surveyQuestion
        self.imageCount = surveyQuestion.images.count
        self.uploadCount = 0
    }

    func didComplete(_ data: Any?) {
        logdata("Upload complete")
        surveyQuestion.images[uploadCount] = data as! ImageData
        uploadCount += 1
        uploadImage()
    }

    func uploadImage() {
        if uploadCount < imageCount {
            uploader = ImageUploader(imageData: surveyQuestion.images[uploadCount])
            uploader?.delegate = self
            uploader?.upload()
        } else {
            self.delegate?.didComplete(self.surveyQuestion)
        }
    }
}

class ProcessSurveyQuestions: DownloadDelegate {
    var surveyQuestions: [SurveyQuestion]
    let questionCount: Int
    var processedCount: Int
    var delegate: DownloadDelegate?
    var processImages: ProcessImages?

    init(questions: [SurveyQuestion]) {
        self.surveyQuestions = questions
        self.questionCount =  questions.count
        self.processedCount = 0
    }

    func didComplete(_ data: Any?) {
        logdata("image processing complete")
        surveyQuestions[processedCount] = data as! SurveyQuestion
        processedCount += 1
        processQuestion()
    }

    func processQuestion() {
        if processedCount < questionCount {
            processImages = ProcessImages(of: surveyQuestions[processedCount])
            processImages?.delegate = self
            processImages?.uploadImage()
        } else {
            logdata("process question complted")
            self.delegate?.didComplete(surveyQuestions)
        }
    }
}
