//
//  ManageSurveyViewController.swift
//  Vreeke Survey
//
//  Created by Alokin on 31/01/19.
//  Copyright © 2019 Alokin. All rights reserved.
//

import UIKit
import CoreData

// order surveys by updated date

class ManageSurveyViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, ManageSurveyCellDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var newSurveyButton: UIButton!
    @IBOutlet weak var submitAllButton: UIButton!
    @IBOutlet weak var headerView: UIView!
    var responses: [Any]!

    @IBAction func submitAllSurveys() {
        var responses = self.responses as! [DBResponse]
        responses = responses.filter({ $0.completed == true })
        if responses.count > 0 {
            appData.setCompletedSurveys(responses)
            self.performSegue(withIdentifier: "submitAllSurveys", sender: self)
        } else {
            let alert = UIAlertController(title: "Warning", message: "Could not find any completed surveys for submission", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }

    }

    override func viewDidLoad() {
        logdata("msvdl", #function)
        super.viewDidLoad()
        for button in [newSurveyButton, submitAllButton] {
            button!.layer.cornerRadius = (button?.frame.height)! / 2
            button?.layer.borderWidth = 1
        }
        newSurveyButton.layer.borderColor = UIColor.black.cgColor
        submitAllButton.layer.borderColor = UIColor.red.cgColor
//        NotificationCenter.default.addObserver(self, selector: #selector(continueSurvey), name: .continueSurvey, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(submitSurvey), name: .manageToSubmit, object: nil)
        responses = database.readData(from: "Response", with: "submitted=%@", arguments: [false], sortBy: [NSSortDescriptor(key: "date", ascending: false)])
        responses = (responses.map({(response: Any) -> DBResponse in
            return getDBResponseObject(response as! NSManagedObject)
        }))
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = false
//        tableView.layer.masksToBounds = false
//        tableView.setShadow()
        headerView.setShadow()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let responseCount =  responses.count
        logdata("responseCount", responseCount)
        return responseCount
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell") as! ManageSurveyCell
        DispatchQueue.main.async {
            if indexPath.row % 2 == 0 {
                cell.contentView.backgroundColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)
            } else {
                cell.contentView.backgroundColor = .white
            }
            cell.delegate = self
            cell.indexPath = indexPath
            cell.dbData = (self.responses![indexPath.row] as! DBResponse)
            cell.updateCellData()
        }

        //cell.indexPath = indexPath
        // cell.dbData = (responses![indexPath.row] as! DBResponse)
//        cell.dbData = getDBResponseObject(responses![indexPath.row] as! NSManagedObject)
        // cell.updateCellData()
//        cell.setShadow()
//        cell.layer.masksToBounds = false
        return cell
    }

    func continueSurvey() {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "continueSurvey", sender: self)
        }
    }

    func submitSurvey() {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "manageToSubmit", sender: self)
        }
    }

    func deleteSurvey(indexpath: IndexPath) {
        DispatchQueue.main.async {
            self.responses.remove(at: indexpath.row)
            self.tableView.deleteRows(at: [indexpath], with: .automatic)
            self.tableView.reloadData()
        }
    }

}

protocol ManageSurveyCellDelegate {
    func deleteSurvey(indexpath: IndexPath)
    func submitSurvey()
    func continueSurvey()
}

class ManageSurveyCell: UITableViewCell {
    @IBOutlet weak var dealerID: UILabel!
    @IBOutlet weak var timestamp: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!

    var indexPath: IndexPath?
    var delegate: ManageSurveyCellDelegate?
    var dbData: DBResponse?

    func updateCellData() {
        dealerID.text = dbData?.response.dealer_id
        timestamp.text = getDateTimeString((dbData?.date)!)

        continueButton.layer.cornerRadius = continueButton.frame.height / 2
        continueButton.layer.borderWidth = 1

        if (dbData?.completed)! {
            logdata("COMPLETED", indexPath)
            continueButton.setTitle("SUBMIT", for: .normal)
            continueButton.layer.borderColor = UIColor.red.cgColor
            continueButton.backgroundColor = UIColor.red
            continueButton.setTitleColor(.white, for: .normal)
            continueButton.addTarget(self, action: #selector(submitSurvey), for: .touchUpInside)
        } else {
            // action to render questions again
            logdata("else", indexPath)
            continueButton.setTitle("CONTINUE", for: .normal)
            continueButton.backgroundColor = .white
            continueButton.layer.borderColor = UIColor.black.cgColor
            continueButton.setTitleColor(.black, for: .normal)
            continueButton.addTarget(self, action: #selector(continueSurvey), for: .touchUpInside)
        }
        deleteButton.addTarget(self, action: #selector(deleteSurvey), for: .touchUpInside)
    }

    @objc func deleteSurvey() {
        database.deleteData(from: "Response", with: "date=%@", arguments: [(dbData?.date)!])
        self.delegate?.deleteSurvey(indexpath: indexPath!)
    }

    @objc func submitSurvey() {
        appData.setDBResponse(dbData!)
        self.delegate?.submitSurvey()
//        postNotification(.manageToSubmit, userInfo: nil)
    }

    @objc func continueSurvey() {
        logdata("continue called")
        appData.setDBResponse(dbData!)
        appData.setSurveyQuestions(from: (dbData?.response)!)
        self.delegate?.continueSurvey()
//        postNotification(.continueSurvey, userInfo: nil)
    }
}
