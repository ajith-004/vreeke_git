//
//  Utils.swift
//  Vreeke Survey
//
//  Created by Alokin on 15/01/19.
//  Copyright © 2019 Alokin. All rights reserved.
//

import Foundation
import UIKit
import CoreData

// Common Objects
let BUILD_DATE = "08.19.2019"
let VERSION_TAG = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String + " \(BUILD_DATE)"
let decoder = JSONDecoder()
let encoder = JSONEncoder()
let appDelegate = UIApplication.shared.delegate as? AppDelegate
let managedContext = appDelegate!.persistentContainer.viewContext
let appData = AppData.sharedInstance
let fileManager = FileManager()
// Fonts
let montserrat: UIFont = UIFont(name: "Montserrat-Regular", size: 30)!
let buttonRed = UIColor(red:0.95, green:0.01, blue:0.00, alpha:1.0)
let buttonGrey = UIColor(red:0.85, green:0.85, blue:0.85, alpha:1.0)
let borderColor = UIColor(red:0.85, green:0.85, blue:0.85, alpha:0.7)


//let SERVER_URL: String = "http://192.168.1.5:8080/api/"
//    let SERVER_URL: String = "http://192.168.1.3:8000/api/"
//    let SERVER_URL: String = "http://192.168.1.13:8000/api/"
//let API_TOKEN: String = "qwerty123456"

let SERVER_URL: String = "https://honda-survey.fronteers.in/api/"
let API_TOKEN: String = "DTybEFpWrAKfKFQvE77bEXnd3W5xUYX9"



func getQuestions() {
    let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "Question")
    do {
        let result = try managedContext.fetch(fetchRequest)
        logdata("db result")
        for r in result as! [NSManagedObject] {
//            logData(r.value(forKey: "question"))
            let js = r.value(forKey: "question") as! String
            let d = try decoder.decode(SurveyData.self, from: js.data(using: .utf8)!)
            logdata(d)
            managedContext.delete(r)
            do {
                try managedContext.save()
            } catch let err {
                logdata(err)
            }
        }
    } catch let err {
        logdata("Error", err)
    }
}


//JSON and Data helpers

func dataToString(_ data: Data) -> String {
    return String(data: data, encoding: .utf8)!
}

func stringToData(_ text: String) -> Data {
    return text.data(using: .utf8)!
}

func NSManagedObjectToString(_ object: NSManagedObject, key: String) -> String {
    return object.value(forKey: key) as! String
}

func NSManagedObjectToData(_ object: NSManagedObject, key: String) -> Data {
    let text: String = NSManagedObjectToString(object, key: key)
    return stringToData(text)
}

// Logging

func logdata(_ data: Any...) {
    print(data, terminator: "\n\n\n\n\n")
}

//Notifications

func postNotification(_ name: Notification.Name, userInfo: [AnyHashable: Any]?) {
    NotificationCenter.default.post(name: name, object: nil, userInfo: userInfo)
}

// Layout helpers

func addSpacing(_ value: CGFloat, offset: CGFloat = 10) -> CGFloat {
    return value + offset
}


// Singletons

class AppData {
    static let sharedInstance = AppData()
    var dealerId: String
    var displayId: String
    var surveyQuestions: [SurveyQuestion]?
    var surveyData: Data?
    var surveyResponse: SurveyResponse?
    var dbResponse: DBResponse?
    var completedSurveys: [DBResponse]?
    var dealerList: DealerList?
    var dealerData: DealerData?

    private init () {
        self.dealerId = ""
        self.displayId = ""
    }

    func setLoginData(_ loginData: LoginData) {
        logdata("setLoginData called", loginData)
        self.dealerId = loginData.dealerId
        self.displayId = loginData.displayId
    }

    func setDealerList(_ data: DealerList) {
        self.dealerList = data
    }

    func setDealerData(_ data: DBResponse) {
        let response =  data.response
        self.dealerData = DealerData(dealerID: response.dealer_id, displayID: response.display_id!, repID: response.rep_id)
//        self.dealerData?.dealerID = data.response.dealer_id
//        self.dealerData?.repID = data.response.rep_id
//        self.dealerData?.displayID = data.response.display_id!
//        logdata(#function, data, self.dealerData)
    }

    func clearDealerInfo() {
        self.dealerList = nil
        self.dealerData = nil
    }

    func setSurveyResponse(_ response: SurveyResponse) {
        self.surveyResponse = response
    }

    func setDBResponse(_ dbResponse: DBResponse) {
        self.dbResponse = dbResponse
        self.surveyResponse = dbResponse.response
        self.setDealerData(dbResponse)
        logdata(#function, self.dbResponse)
    }

    func clearDBResponse(clearDealerInfo: Bool = true) {
        self.dbResponse = nil
        self.surveyResponse = nil
        if clearDealerInfo {
            self.clearDealerInfo()
        }
    }
    
    func deleteUserUploadedImages() {
        let urlList = self.dbResponse?.response.getImageURLs()
        for urls in urlList ?? [] {
            for url in urls {
                if url.contains("userUploads") {
                    deleteFileAtPath(path: url)
                }
            }
        }
    }

    func setCompletedSurveys(_ data: [DBResponse]) {
        self.completedSurveys = data
    }

    func clearCompletedSurveys() {
        self.completedSurveys = nil
    }

    func clearSurveyResponse() {
        self.surveyResponse = SurveyResponse(dealer: "", rep: "", response: [SurveyQuestion](), displayID: "")
    }

    func setSurveyQuestions(using surveyData: Data) {
        /**
         test
         */
        do {
            let decodedData = try decoder.decode(SurveyData.self, from: surveyData)
            self.surveyQuestions = decodedData.result
            self.surveyData = surveyData
        } catch let err {
            logdata(#file, #function, err)
        }
    }

    func setSurveyQuestions(from surveyResponse: SurveyResponse) {
        self.surveyQuestions = surveyResponse.survey_response
    }

}

// Documents directory helpers

func dropLeadingSlash(_ path: String) -> String {
    var path = path
    if path.hasPrefix("/") {
        path = String(path.dropFirst())
    }
    return path
}

func getDocumentsDirectory() -> URL {
    let paths = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
    let documentsDirectory = paths[0]
    return documentsDirectory
}

func getFileSavePath(_ path: String) -> URL {
    let fileURL = getDocumentsDirectory().appendingPathComponent(dropLeadingSlash(path))
//    logData(#function, fileURL)
    return fileURL
}

func fileExistsAtPath(_ path: String) -> Bool {
    return fileManager.fileExists(atPath: getFileSavePath(path).path)
}

func getFileData(_ path: String) -> Data? {
    if fileExistsAtPath(path) {
        logdata("get file data -> File exists", getFileSavePath(path))
        //        let data = fileManager.contents(atPath: getFileSavePath(path))
        do {
            //            let attr = try fileManager.attributesOfItem(atPath: getFileSavePath(path).path)
            //            print(attr)
            //            let readable = fileManager.isReadableFile(atPath: getFileSavePath(path).path)
            //            print(readable)
            let data = try Data(contentsOf: getFileSavePath(path), options: .mappedIfSafe)
            //            pprint("File Data -->", data)
            logdata(#function, data)
            return data
        } catch let error {
            logdata(error.localizedDescription)
            return nil
        }
    } else {
        logdata("File missing at path")
        return nil
    }
}

func getImageForPath(_ path: String) -> UIImage {
    if let data = getFileData(path) {
        logdata("Non nil data")
        return UIImage(data: data)!
    } else {
        return UIImage(named: "dummy")!
    }
}

func deleteFileAtPath(path: String) {
    logdata(#function, path)
    do {
        try fileManager.removeItem(at: getFileSavePath(path))
    } catch let error {
        logdata(#function, error)
    }
}

func saveFileToPath(path: String, data: Data) {
    logdata(#function, path)
    var slashIndices = path.indices(of: "/")
    var attributes = [FileAttributeKey : Any]()
    attributes[.posixPermissions] = 0o777
    if slashIndices.count > 0 {
        let dirPath: String = String(path.prefix(slashIndices.last! + 1))
        // Create directory structure
        do {
            try fileManager.createDirectory(atPath: getFileSavePath(dirPath).path, withIntermediateDirectories: true, attributes: attributes)
            try data.write(to: getFileSavePath(path)) // Using FileManager to create file at path always fails. So write data directly to URL
        } catch let error {
            logdata(error.localizedDescription)
        }
    } else {
        logdata("create file", path)
        fileManager.createFile(atPath: getFileSavePath(path).path, contents: data, attributes: attributes)
        logdata("exists", fileExistsAtPath(path))
    }
}

func getDateTimeString(_ date: Date = Date()) -> String {
    let dateFormatter: DateFormatter = DateFormatter()
    dateFormatter.dateStyle = .medium // Jan 01, 2019
    dateFormatter.timeStyle = .short // 10:00 AM
    dateFormatter.locale = Locale(identifier: "en_US")
    return dateFormatter.string(from: date)
}

func getDBResponseObject(_ data: NSManagedObject) -> DBResponse {
    let date = data.value(forKey: "date") as! Date
    let response = try! decoder.decode(SurveyResponse.self, from: NSManagedObjectToData(data, key: "response"))
    let completed = data.value(forKey: "completed") as! Bool
    let submitted = data.value(forKey: "submitted") as! Bool
    return DBResponse(date: date, response: response, completed: completed, submitted: submitted)
}

func getPendingSurveyCount() -> Int {
    return database.readData(from: "Response", with: "submitted=%@", arguments: [false]).count
}

func getPendingSurveyText(_ count: Int) -> String {
    let text = count > 1 ? "Surveys" : "Survey"
    return "\(count) \(text) pending"
}
