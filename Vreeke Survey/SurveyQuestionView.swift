//
//  SurveyQuestionView.swift
//  Vreeke Survey
//
//  Created by Alokin on 23/01/19.
//  Copyright © 2019 Alokin. All rights reserved.
//

import UIKit

class SurveyQuestionView: UIView {
    var questionNumber: UILabel?
    var questionTitle: UILabel?
    var questionTitleX: CGFloat?
    var questionTitleWidth: CGFloat?
    var questionDescription: UILabel?
    var imageDescription: UILabel?
    var questionText: String? // Implement
    var responseData: SurveyQuestion?
    var questionOptionsView: SurveyQuestionOptionsView?
    var questionImagesView: SurveyQuestionImageWrapperView?
    var imageTitleX, imageSectionX, questionTop: CGFloat!

    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }
    
    func customInit() {
        /*
         bounds - view’s location and size in its own coordinate system
         frame - view’s location and size in its superview’s coordinate system
        */

//        self.backgroundColor = UIColor.yellow

        questionNumber = UILabel(frame: CGRect(x: 30, y: 25, width: 50, height: 50))
        questionNumber?.font = UIFont.systemFont(ofSize: 30)
        questionNumber?.numberOfLines = 0
//        self.addSubview(questionNumber!)

        questionTitleX = addSpacing(questionNumber!.frame.maxX, offset: 20)
        questionTitleX = 90
        questionTitleWidth = self.bounds.width - questionTitleX! - 25
        
        if UIScreen.main.bounds.width < 768 {
            imageTitleX = 30
            imageSectionX = 0
            questionTop = 30
        } else {
            imageTitleX = questionTitleX!
            imageSectionX = questionTitleX!
            questionTop = 40
        }
        
        questionTitle = UILabel(frame: CGRect(x: questionTitleX!, y: questionTop, width: questionTitleWidth!, height: 200))
        questionTitle?.numberOfLines = 0
//        self.addSubview(questionTitle!)
        self.backgroundColor = .white

    }
    
    func updateWithData(_ surveyQuestion: SurveyQuestion, index: Int) {
        let questionWrapper: UIView = UIView(frame: CGRect(x: self.frame.minX, y: self.frame.minY
            , width: self.frame.width, height: self.frame.height))
        questionWrapper.backgroundColor = UIColor(red:0.96, green:0.96, blue:0.96, alpha:1.0)

        questionNumber!.text =  prefixString(String(index + 1))
        questionNumber?.font = UIFont(name: "Montserrat-Black", size: 32)!
        questionNumber?.sizeToFit()

        questionTitle!.text = surveyQuestion.title
        questionTitle!.font = UIFont(name: "Montserrat-Bold", size: 16)!
        questionTitle?.sizeToFit()

        questionWrapper.addSubview(questionNumber!)
        questionWrapper.addSubview(questionTitle!)
        
        // Description and other ui elements depend on question title's postition. Declare them here

        questionDescription = UILabel(frame: CGRect(x: questionTitleX!, y: addSpacing(questionTitle!.frame.maxY), width: questionTitleWidth!, height: 200))
        questionDescription?.numberOfLines = 0
        questionDescription?.text = surveyQuestion.description
        questionDescription?.font = UIFont(name: "Montserrat-Regular", size: 16)
        questionDescription?.sizeToFit()
//        self.addSubview(questionDescription!)
        questionWrapper.addSubview(questionDescription!)

        questionOptionsView = SurveyQuestionOptionsView(frame: CGRect(x: questionTitleX!, y: addSpacing((questionDescription?.frame.maxY)!, offset: 30), width: questionTitleWidth!, height: 50))
        questionOptionsView!.renderOptions(surveyQuestion.options)
//        questionOptionsView!.sizeToFit()
//        self.addSubview(questionOptionsView!)
        questionWrapper.addSubview(questionOptionsView!)
        let qwframe = questionWrapper.bounds
        questionWrapper.frame = CGRect(x: qwframe.minX, y: qwframe.minY, width: qwframe.width, height: addSpacing(questionOptionsView!.frame.maxY, offset: 30))
        self.addSubview(questionWrapper)

        var lastView = questionWrapper
        var imageTitleX, imageSectionX: CGFloat
        if UIScreen.main.bounds.width < 768 {
            imageTitleX = 30
            imageSectionX = 0
        } else {
            imageTitleX = questionTitleX!
            imageSectionX = questionTitleX!
        }
        // TODO: image desc x postion, and width, button section, image preview size
        if surveyQuestion.image_description.count > 0 {
            /*
//            let stackView = UIStackView()
//            let stackView = UIStackView(frame: CGRect(x: imageTitleX, y: addSpacing(lastView.frame.maxY, offset: 25), width: self.frame.width - imageTitleX - 25, height: 0))
//            stackView.axis = .vertical
//            stackView.distribution = .fill
            imageDescription = UILabel()
//            imageDescription = UILabel(frame: CGRect(x: questionTitleX!, y: addSpacing(lastView.frame.maxY, offset: 25), width: questionTitleWidth!, height: 50))
            imageDescription!.text = surveyQuestion.image_description
            imageDescription!.numberOfLines = 0
            imageDescription?.font = UIFont(name: "Montserrat-Bold", size: 16)!
            imageDescription!.sizeToFit()
//            stackView.addArrangedSubview(imageDescription!)
            
            let errorMessage = UILabel()
            errorMessage.text = "Please upload atleast one image to proceed."
            errorMessage.numberOfLines = 0
            errorMessage.font = UIFont(name: "Montserrat-Regular", size: 14)!
            errorMessage.textColor = buttonRed
            errorMessage.isHidden = true
            errorMessage.sizeToFit()
//            stackView.addArrangedSubview(errorMessage)
            
//            let errorMessage1 = UILabel()
//            errorMessage1.text = "Please upload atleast one image to proceed"
//            errorMessage1.numberOfLines = 0
//            errorMessage1.font = UIFont(name: "Montserrat-Regular", size: 14)!
//            errorMessage1.textColor = buttonRed
//            errorMessage1.sizeToFit()
//            stackView.addArrangedSubview(errorMessage1)
            
//            let errorMessage2 = UILabel()
//            errorMessage2.text = "Please upload atleast one image to proceed"
//            errorMessage2.numberOfLines = 0
//            errorMessage2.font = UIFont(name: "Montserrat-Regular", size: 14)!
//            errorMessage2.textColor = buttonRed
//            errorMessage2.sizeToFit()
//            stackView.addArrangedSubview(errorMessage2)
            
            let imageDescriptionFrame = imageDescription!.frame
            let spacingView = UIView()
            spacingView.bounds = CGRect(x: 0, y: 0, width: imageDescriptionFrame.width, height: 25)
//            spacingView.backgroundColor = buttonRed
//            let spacingView = UIView(frame: CGRect(x: imageDescriptionFrame.minX, y: imageDescriptionFrame.maxY, width: imageDescriptionFrame.width, height: 25))
//            stackView.addArrangedSubview(spacingView)
//            stackView.sizeToFit()
            let stackView = UIStackView(arrangedSubviews: [imageDescription!, errorMessage, spacingView])
//            stackView.frame = CGRect(x: imageTitleX, y: addSpacing(lastView.frame.maxY, offset: 25), width: self.frame.width - imageTitleX - 25, height: imageDescription!.heightAnchor.)
            stackView.topAnchor.anchorWithOffset(to: (imageDescription?.topAnchor)!)
            stackView.bottomAnchor.anchorWithOffset(to: spacingView.bottomAnchor)
            stackView.axis = .vertical
            stackView.distribution = .fill
            stackView.layoutIfNeeded()
            self.addSubview(stackView)
            logdata("stackview", stackView.frame)
            lastView = stackView
            */
            let imageDescriptionWidth: CGFloat = self.bounds.width - imageTitleX - 25
            imageDescription = UILabel(frame: CGRect(x: imageTitleX, y: addSpacing(lastView.frame.maxY, offset: 25), width: imageDescriptionWidth, height: 50))
            imageDescription!.text = surveyQuestion.image_description
            imageDescription!.numberOfLines = 0
            imageDescription?.font = UIFont(name: "Montserrat-Bold", size: 16)!
            imageDescription!.sizeToFit()
            self.addSubview(imageDescription!)
            let imageDescriptionFrame = imageDescription!.frame
            let errorMessage = ErrorLabel(frame: CGRect(x: imageDescriptionFrame.minX, y: imageDescriptionFrame.maxY, width: imageDescriptionWidth, height: 25))
            errorMessage.text = "Please upload atleast one image to proceed."
            errorMessage.numberOfLines = 0
            errorMessage.font = UIFont(name: "Montserrat-Regular", size: 14)!
            errorMessage.textColor = buttonRed
            errorMessage.isHidden = true
            errorMessage.sizeToFit()
            self.addSubview(errorMessage)
            let spacingView = UIView(frame: CGRect(x: imageDescriptionFrame.minX, y: imageDescriptionFrame.maxY, width: imageDescriptionWidth, height: 25))
            self.addSubview(spacingView)
            lastView = spacingView
        }

        questionImagesView = SurveyQuestionImageWrapperView(frame: CGRect(x: imageSectionX, y: lastView.frame.maxY, width: UIScreen.main.bounds.width - imageSectionX, height: 100))
//        questionImagesView = SurveyQuestionImageWrapperView(frame: CGRect(x: questionTitleX!, y: lastView.frame.maxY, width: UIScreen.main.bounds.width - questionTitleX!, height: 100))
        questionImagesView!.renderImageSection(surveyQuestion.images)
        questionOptionsView?.delegate = questionImagesView
        self.addSubview(questionImagesView!)
        
        let subViewCount = self.subviews.count
        let lastSubView = self.subviews[subViewCount - 1]
        self.frame = CGRect(x: self.frame.minX, y: self.frame.minY, width: self.frame.width, height: lastSubView.frame.maxY)
    }

    func prefixString(_ text: String) -> String {
        if text.count < 2 {
            return "0\(text)"
        }
        return text
    }
//
    func getResponseData() -> SurveyQuestion {
        responseData = SurveyQuestion()
        responseData?.title = (questionTitle?.text)!
        responseData?.description = (questionDescription?.text)!
        responseData?.options = (questionOptionsView?.getOptionData())!
        responseData?.images = (questionImagesView?.getImageData())!
        responseData?.imageValidation = questionImagesView?.validateImages() ?? false
        responseData?.image_description = imageDescription?.text ?? ""
        return responseData!
    }
    
    func getErrorMessageWrapper() -> [UIView] {
//        return self.subviews.filter({ $0 is UIStackView })
        return self.subviews.filter({ $0 is ErrorLabel })
    }

}
