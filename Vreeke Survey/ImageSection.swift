//
//  ImageSection.swift
//  Vreeke Survey
//
//  Created by Alokin on 28/01/19.
//  Copyright © 2019 Alokin. All rights reserved.
//

import Foundation
import UIKit

class ImageSection: UIView {
    
    var imageTitle: UILabel?
    var uploadButton: OptionButton?
    var imagePreview: UIImageView?
    var overlay: UIImageView?
    var sampleTrigger: UIView?
    var newImage: Bool = false
    var imageRequiredSelected: Bool = false
    var imageData: ImageData?
    var wrapperView: SurveyQuestionImageWrapperView!
    var imageSectionIndex: Int!

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    /*
     TODO:
     -----
     handle button style changes
     pass button in notification
    */

    func initWithData(_ data: ImageData, index: Int) {
        imageSectionIndex = index
        wrapperView =  self.superview as! SurveyQuestionImageWrapperView
        
        var offset,
            uploadButtonOffset,
            uploadButtonWidth,
            uploadButtonHeight,
            uploadButtonFontSize,
            imagePreviewWidth,
            imagePreviewHeight: CGFloat
        uploadButtonHeight = 50
        if UIScreen.main.bounds.width < 768 {
            offset = 30
            uploadButtonOffset = 30
            uploadButtonWidth = 135
//            uploadButtonHeight = 38
            imagePreviewWidth = 182
            imagePreviewHeight = 140
            uploadButtonFontSize = 14
        } else {
            offset = 10
            uploadButtonOffset = 40
            uploadButtonWidth = 200
//            uploadButtonHeight = 50
            imagePreviewWidth = 232
            imagePreviewHeight = 180
            uploadButtonFontSize = 16
        }
        
        imageData = data
        imageTitle = UILabel(frame: CGRect(x: addSpacing(self.bounds.minX, offset: offset), y: addSpacing(self.bounds.minY, offset: 20), width: self.bounds.width - addSpacing(imagePreviewWidth, offset: 20), height: 50))
        imageTitle!.text = data.title
        imageTitle!.numberOfLines = 2
        imageTitle!.font = UIFont(name: "Montserrat-Regular", size: 16)!
        imageTitle!.sizeToFit()
        self.addSubview(imageTitle!)

        uploadButton = OptionButton(type: .custom)
        uploadButton!.frame = CGRect(x: (imageTitle?.frame.minX)!, y: addSpacing(self.bounds.maxY, offset: -(uploadButtonHeight + 10)), width: uploadButtonWidth, height: uploadButtonHeight)
        uploadButton!.setTitle("TAKE PHOTO", for: .normal)
        uploadButton?.setTitleColor(.white, for: .normal)
        uploadButton?.titleLabel!.font = UIFont(name: "Montserrat-Bold", size: uploadButtonFontSize)!
        logdata("Init image section", data)
        if data.requiredOptionSelected ?? false {
            uploadButton?.backgroundColor = buttonRed
            wrapperView.hasImage[imageSectionIndex] = false
            imageRequiredSelected = true
        } else {
            uploadButton?.backgroundColor = buttonGrey
            wrapperView.hasImage[imageSectionIndex] = true
            imageRequiredSelected = false
        }
        uploadButton?.setRoundedCorners()
        uploadButton!.addTarget(self, action: #selector(takePhoto), for: .touchUpInside)
        self.addSubview(uploadButton!)

        let frame =  CGRect(x: self.bounds.width - imagePreviewWidth, y: 0, width: imagePreviewWidth, height: imagePreviewHeight)
//        imagePreview = UIImageView(frame: CGRect(x: self.bounds.width - 232, y: 0, width: 232, height: 180))
        imagePreview = UIImageView(frame: frame)
        imagePreview?.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.8)
        imagePreview?.contentMode = .scaleAspectFill
        imagePreview?.clipsToBounds = true
        imagePreview?.image = getImageForPath(data.url)
        self.addSubview(imagePreview!)

        // overlayview
//        overlay = UIImageView(frame: CGRect(x: self.bounds.width - 232, y: 0, width: 232, height: 180))
        overlay = UIImageView(frame: frame)
        overlay?.isUserInteractionEnabled = true
        overlay?.contentMode = .scaleAspectFill
        overlay?.clipsToBounds = true
        let gesture = UITapGestureRecognizer(target: self, action: #selector(showPopup))
        overlay?.addGestureRecognizer(gesture)
        if !data.url.contains("userUploads") {
            overlay?.image = UIImage(named: "see_sample")
            uploadButton?.imageUploaded = false
        } else {
            updateUploadButton()
        }
        self.addSubview(overlay!)

//        self.backgroundColor = UIColor.red

        let border = UIView(frame: CGRect(x: self.bounds.minX, y: self.bounds.minY, width: self.bounds.width - imagePreviewWidth, height: 2))
        border.backgroundColor = borderColor
        self.addSubview(border)
        logdata("self.bounds", self, self.bounds)
    }

    @objc func showPopup() {
        postNotification(.showPopup, userInfo: ["imageView": imagePreview])
    }

    @objc func takePhoto() {
        postNotification(.captureImage, userInfo: ["imageSection": self])
    }

    func postImageCapture(_ image: UIImage) {
        imagePreview?.image = image
        newImage = true
        overlay?.image = nil
        updateUploadButton()
    }
    /// Updates upload button style for new images
    func updateUploadButton() {
        uploadButton?.setTitle("RETAKE PHOTO", for: .normal)
        uploadButton?.setTitleColor(.black, for: .normal)
        uploadButton?.backgroundColor = UIColor.white
        uploadButton?.layer.borderColor = UIColor.black.cgColor
        uploadButton?.imageUploaded = true
        wrapperView.hasImage[imageSectionIndex] = true
    }
    
    func selectedImageRequiredOption() {
        if !hasUploadedImage() {
            uploadButton?.backgroundColor = buttonRed
            imageRequiredSelected = true
            wrapperView.hasImage[imageSectionIndex] = false
        } else {
            wrapperView.hasImage[imageSectionIndex] = true
        }
    }
    
    func unselectedImageRequiredOption() {
        if !hasUploadedImage() {
            uploadButton?.backgroundColor = buttonGrey
            imageRequiredSelected = false
        }
        wrapperView.hasImage[imageSectionIndex] = true
    }
    
    func hasUploadedImage() -> Bool {
        return uploadButton?.imageUploaded ?? false
    }

    func requestImageData() -> ImageData {
        var data = ImageData()
        data.title = (imageTitle?.text)!
        data.newImage = newImage
        data.requiredOptionSelected = imageRequiredSelected
        
        if newImage {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            dateFormatter.timeStyle = .short
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
            dateFormatter.locale = Locale(identifier: "en_US")
            let fileName: String = "userUploads/" + dateFormatter.string(from: Date()) + String(Double.random(in: 0.0...5.0)) + ".jpg"
            saveFileToPath(path: fileName, data: (imagePreview?.image?.jpegData(compressionQuality: 0.5))!)
//            data.url = getFileSavePath(fileName).absoluteString
            data.url = fileName
            logdata(#function, data.url, fileExistsAtPath(fileName))
        } else {
            data.url = (imageData?.url)!
        }
        logdata(#file, #function, data)
        return data
    }
}
