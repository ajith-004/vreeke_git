//
//  Request.swift
//  Vreeke Survey
//
//  Created by Alokin on 15/01/19.
//  Copyright © 2019 Alokin. All rights reserved.
//

import Foundation
import CoreData

class Request {
    let session: URLSession

    let HTTP_SUCCESS: Int = 200
    let HTTP_BAD_REQUEST: Int = 400
    let HTTP_UNAUTHORIZED: Int =  401
    let HTTP_PAGE_NOT_FOUND: Int = 404
    
//    let API_URLS = [
//        "loginDev": "login",
//        "login": "dealer-login",
//        "questionDev": "question",
//        "questions": "questions"
//    ]
    
    init() {
        self.session = URLSession(configuration: .default)
    }
    
    func generateURL(_ endpoint: String, appendEndpoint: Bool) -> URL {
        if appendEndpoint {
            return URL(string: SERVER_URL)!.appendingPathComponent(endpoint + "/")
        } else {
            return URL(string: endpoint)!
        }
    }
    
    func generateRequest(_ endpoint: String, appendEndpoint:Bool = true, method: String = "GET", data: Data? = nil, isJSON: Bool = false) -> URLRequest {
        var request = URLRequest(url: generateURL(endpoint, appendEndpoint: appendEndpoint))
        request.httpMethod = method
        request.addValue(API_TOKEN, forHTTPHeaderField: "api-token")
        if isJSON {
            request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        }
        if (data != nil) {
            request.httpBody = data
        }
        return request
    }
    
    func login(data: Data) {
        self.session.dataTask(with: generateRequest("login", method: "POST", data: data)){(data, response, error) in
//            print("data", String(data: data!, encoding: .utf8), "response", response, "error", error, terminator: "\n\n\n\n\n")
            if error != nil {
                logdata(error?.localizedDescription)
                return
            }
            guard let data =  data else {
                return
            }
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode == self.HTTP_SUCCESS {
                    // login and call questions api
                    logdata("Login Sucess")
                    do {
                        let loginData = try decoder.decode(LoginData.self, from: data)
                        AppData.sharedInstance.setLoginData(loginData)
                    } catch let err{
                        print("Error", err)
                    }
                    postNotification(.loginSuccess, userInfo: nil)
                } else {
                    // show error message
                    logdata("Login failed")
                    postNotification(.loginFailed, userInfo: nil)
                }
            }
        }.resume()
    }
    
    func getSurvey() {
        logdata(#file, #function)
        self.session.dataTask(with: generateRequest("questions")){(data, response, error) in
            if error != nil {
                return
            }
            guard var data = data else {
                return
            }
            logdata(#file, #function, response)
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode == self.HTTP_SUCCESS {
                    do {
                        logdata(String(data: data, encoding: .utf8))
                        postNotification(.getSurveyDataSuccess, userInfo: ["data": data])
//                        let decodedData = try decoder.decode(SurveyData.self, from: data)
//                        AppData.sharedInstance.surveyQuestions = decodedData.result
//                        logData(decodedData)
//                        let questionEntity = NSEntityDescription.entity(forEntityName: "Question", in: managedContext)!
//                        let question = NSManagedObject(entity: questionEntity, insertInto: managedContext)
//                        question.setValue(String(data: data, encoding: .utf8), forKey:"question")
//                        try managedContext.save()
                    } catch let err {
                        logdata("Fetch survery error", err)
                    }
                }
            }
            
        }.resume()
    }

    func downloadImage(_ path: String) {
        DispatchQueue.global(qos: .userInitiated).sync {
            self.session.dataTask(with: generateRequest(path, appendEndpoint: false)){(data, response, error) in
                if error == nil {
                    if self.isSuccessResponse(response) {
                        saveFileToPath(path: path, data: data!)
                        logdata("Success", path, data, response)
                        postNotification(.downloadImageComplete, userInfo: nil)
                    }
                } else {
                    logdata("file save failed", path)
                }
            }.resume()
        }
    }

    func getDealersList() {
        self.session.dataTask(with: generateRequest("dealer-display")) {(data, response, error) in
            logdata(#file, #function)
            logdata(data, response, error)
            if error == nil {
                if self.isSuccessResponse(response) {
                    database.updateOrCreateData(of: "Dealers", with: nil, arguments: nil, data: ["dealerList": dataToString(data!)])
                    postNotification(.dealerListDownloaded, userInfo: nil)
                } else {
                    postNotification(.dealerListDownloadFailed, userInfo: nil)
                }
            } else {
                postNotification(.dealerListDownloadFailed, userInfo: nil)
            }
        }.resume()
    }

    func submitResponse(data: Data, submitAll: Bool = false) {
        var successNotification, failureNotification: Notification.Name
        if submitAll {
            successNotification = .submitAllResponse
            failureNotification = .submitAllResponseFailed
        } else {
            successNotification = .submitResponse
            failureNotification = .submitResponseFailed
        }
        self.session.dataTask(with: generateRequest("response", method: "POST", data: data, isJSON: true)) {(data, response, error) in
            if error == nil {
                if self.isSuccessResponse(response) {
                    logdata("data sub success")
                    postNotification(successNotification, userInfo: nil)
                } else {
                    postNotification(failureNotification, userInfo: nil)
                }
            } else {
                postNotification(failureNotification, userInfo: nil)
            }
        }.resume()
    }

    func isSuccessResponse(_ response: URLResponse?) -> Bool {
        if response != nil {
            let httpResponse = response as! HTTPURLResponse
            return httpResponse.statusCode == HTTP_SUCCESS ? true : false
        }
        return false
    }

}

let request = Request()
