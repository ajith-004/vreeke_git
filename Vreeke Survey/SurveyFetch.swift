//
//  SurveyFetch.swift
//  Vreeke Survey
//
//  Created by Alokin on 16/01/19.
//  Copyright © 2019 Alokin. All rights reserved.
//

import UIKit
import CoreData


class SurveyFetch: BaseViewController {
    var imageCount = 0
    var downloadedImageCount = 0
    @IBOutlet weak var progress: UIProgressView!
    @IBOutlet weak var versionTag: UILabel!

    @IBAction func fetchAndSaveToDb(_ sender: Any) {
        request.getSurvey()
    }
    
    @IBAction func fetchFromDb(_ sender: Any) {
        let data = database.readData(from: "Response", with: "date=%@", arguments: nil)
        logdata(#function, data, data.count)
    }
    
    @IBAction func deleteFromDb(_ sender: Any) {
        database.deleteData(from: "Question", with: nil, arguments: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .getSurveyDataSuccess, object: nil)
        NotificationCenter.default.removeObserver(self, name: .downloadImageComplete, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(processSurveyQuestionData), name: .getSurveyDataSuccess, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(downloadComplete), name: .downloadImageComplete, object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        versionTag.text = VERSION_TAG
        logdata(#file, #function)
        /*
         check for net
            yes - fetch data, save to app data and db, download images
            no - load from db to app data, check for all images?
         */
//        NotificationCenter.default.addObserver(self, selector: #selector(processSurveyQuestionData), name: .getSurveyDataSuccess, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(downloadComplete), name: .downloadImageComplete, object: nil)
        if connectedToNetwork() {
            request.getSurvey()
            // transition to new page and render using question data
        } else {
            logdata("No internet connection")
            // To implement
            DispatchQueue.main.async {
                let result = database.readData(from: "Question", with: nil, arguments: nil)
                if result.count > 0 {
                    for data in result as! [NSManagedObject] {
                        let question = data.value(forKey: "question") as! String
                        self.setDataAndRender(using: stringToData(question))
                        self.performSegue(withIdentifier: "showSurvey", sender: nil)
                    }
                } else {
                    logdata("NO data from DB")
                    // show warning, connect to net, no survey data found on device
                    let alert = UIAlertController(title: "Warning", message: "No surveys found on device. Make sure are online to download one", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {_ in
                        self.performSegue(withIdentifier: "returnToHome", sender: self)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }

        }
    }

    @objc func downloadComplete() {
        logdata(#function)
        downloadedImageCount += 1
        DispatchQueue.main.async {
            self.progress.setProgress(Float(self.downloadedImageCount / self.imageCount), animated: true)
            if self.downloadedImageCount == self.imageCount {
                self.performSegue(withIdentifier: "showSurvey", sender: self)
            }
        }
//        progress.setProgress(Float(downloadedImageCount / imageCount), animated: true)
//        if downloadedImageCount == imageCount {
//            logdata("***********************")
//            DispatchQueue.main.async {
//                self.performSegue(withIdentifier: "showSurvey", sender: self)
//            }
//        }
    }
    
    @objc func processSurveyQuestionData(_ notification: NSNotification) {
        logdata(#function)
        let dataDict: [String: Data] = notification.userInfo as! [String: Data]
        let data: Data = dataDict["data"]!
        if database.updateOrCreateData(of: "Question", with: nil, arguments: nil, data: ["question": dataToString(data)]) {
            setDataAndRender(using: data)
        }
    }

    func setDataAndRender(using data: Data) {
        appData.setSurveyQuestions(using: data)
        self.downloadSampleImages()
    }

    func downloadSampleImages() {
        // get images and download
        let collection = appData.surveyQuestions!.map({ $0.images })
        imageCount = collection.map({ $0.count }).reduce(0, {x,y in
            x + y
        })
        for images in collection {
            for image in images {
                // download - if exists pass
                let imagePath = image.url
                if !fileExistsAtPath(imagePath) {
                    logdata("file doesn't exist", imagePath)
                    DispatchQueue.main.async {
                        request.downloadImage(imagePath)
                    }
                } else {
                    logdata(#function, "File exists", imagePath)
                    DispatchQueue.main.async {
                        self.downloadComplete()
                    }
                }
            }
        }
    }
}
