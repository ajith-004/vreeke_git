//
//  Codables.swift
//  Vreeke Survey
//
//  Created by Alokin on 24/01/19.
//  Copyright © 2019 Alokin. All rights reserved.
//

import Foundation

struct LoginData: Codable {
    var status, dealerId, displayId: String
    private enum CodingKeys: String, CodingKey {
        case status
        case dealerId = "dealer_id"
        case displayId = "display_id"
    }
}

struct ImageData: Codable {
    var url, title: String
    var newImage: Bool?
    var requiredOptionSelected: Bool?

    init() {
        url = ""
        title = ""
        newImage = false
        requiredOptionSelected =  false
    }

    private enum CodingKeys: String, CodingKey {
        case url = "image"
        case title
        case requiredOptionSelected
    }
}

struct OptionData: Codable {
    var option: String
    var checked, imageRequired: Bool?

    private enum CodingKeys: String, CodingKey {
        case option
        case checked
        case imageRequired = "image_required"
    }
}

struct SurveyQuestion: Codable {
    var description: String
    var title: String
    var image_description: String
    var options: [OptionData]
    var images: [ImageData]
    var imageValidation: Bool?

    init() {
        description = ""
        title = ""
        image_description = ""
        options = [OptionData]()
        images = [ImageData]()
        imageValidation = false
    }
    
    func getImageURLS() -> [String] {
        logdata("SurveyQuestion getImageURLS", images.map{ $0.url })
        return images.map{ $0.url }
    }
}

struct SurveyData: Codable {
    var status: String
    var result: [SurveyQuestion]
}

struct ImageUploadResponse: Codable {
    var id: Int
    var message: String

//    private enum CodingKeys: String, CodingKey {
//        case imageId = "image_id"
//        case message
//    }
}

struct SurveyResponse: Codable {
    var dealer_id, rep_id: String
    var survey_response: [SurveyQuestion]
    var display_id: String?

    init(dealer: String, rep: String, response: [SurveyQuestion], displayID: String) {
        dealer_id = dealer
        rep_id = rep
        survey_response = response
        display_id = displayID
    }
    
    func getImageURLs() -> [[String]] {
        logdata("SurveyResponse getImageURLs", survey_response.map{ $0.getImageURLS() })
        return survey_response.map{ $0.getImageURLS() }
    }
}

struct DBResponse {
    var date: Date
    var response: SurveyResponse
    var completed, submitted: Bool

    init(date: Date, response: SurveyResponse, completed: Bool = false, submitted: Bool = false) {
        self.date = date
        self.response = response
        self.completed = completed
        self.submitted = submitted
    }
}

struct DealerData: Codable {
    var dealerID, displayID: String
    var repID: String?

    private enum CodingKeys: String, CodingKey {
        case dealerID = "dealer_id"
        case displayID = "display"
    }

    init(dealerID: String, displayID: String, repID: String) {
        self.dealerID = dealerID
        self.displayID = displayID
        self.repID = repID
    }
}

struct DealerList: Codable {
    var result: [DealerData]
}
