//
//  Database.swift
//  Vreeke Survey
//
//  Created by Alokin on 17/01/19.
//  Copyright © 2019 Alokin. All rights reserved.
//

import CoreData
import UIKit


class Database {
    let appdelegate: AppDelegate
    let context: NSManagedObjectContext
    
    init() {
        self.appdelegate = UIApplication.shared.delegate as! AppDelegate
        self.context = appdelegate.persistentContainer.viewContext
    }
    
    func getNSManagedObject(for entity: String) -> NSManagedObject? {
        let description = NSEntityDescription.entity(forEntityName: entity, in: context)
        return NSManagedObject.init(entity: description!, insertInto: context)
    }
    
    func saveToEntity(_ object: NSManagedObject, data: [String: Any]) {
        for (key, value) in data {
            object.setValue(value, forKey: key)
        }
    }
    
    func deleteData(from entity: String, with filter: String?, arguments: [Any]?) -> Bool {
        let requestData = readData(from: entity, with: filter, arguments: arguments)
        for data in requestData {
            let dataToDelete = data as! NSManagedObject
            context.delete(dataToDelete)
        }
        do {
            try context.save()
            return true
        } catch let err {
            logdata(#file, #function, err)
            return false
        }
    }
    
    func readData(from entity: String, with filter: String?, arguments: [Any]?, sortBy: [NSSortDescriptor] = []) -> [Any] {
        // fetchRequest.fetchLimit ??
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: entity)
        if filter != nil {
            // Arguments are required for filter
            guard let arguments = arguments else {
                return []
            }
            fetchRequest.predicate = NSPredicate.init(format: filter!, argumentArray: arguments)
        }
        if sortBy.count > 0 {
            fetchRequest.sortDescriptors = sortBy
        }
        do {
            let data = try self.context.fetch(fetchRequest)
            logdata(#file, #function, data)
            return data
        } catch let err {
            logdata(#file, #function, err)
        }
        return []
    }
    // save String data
    func saveData(to entity: String, data: [String: Any]) -> Bool {
        guard let object = getNSManagedObject(for: entity) else {
            return false
        }
        saveToEntity(object, data: data)
        do {
            try context.save()
            return true
        } catch  let err{
            logdata("Save error", err)
            return false
        }
    }
    
    func updateData(of entity: String, with filter: String?, arguments: [Any]?, data: [String: Any]) -> Bool {
        // we need to fetch data to memory before updating it
        let requestData: [Any] = readData(from: entity, with: filter, arguments: arguments)
        if requestData.count > 0 {
            let object: NSManagedObject = requestData[0] as! NSManagedObject
            saveToEntity(object, data: data)
            do {
                try context.save()
                return true
            } catch let err{
                logdata(#file, #function, err)
            }
        }
        return false
    }
    
    func updateOrCreateData(of entity: String, with filter: String?, arguments: [Any]?, data: [String: String]) -> Bool {
        return updateData(of: entity, with: filter, arguments: arguments, data: data) ? true : saveData(to: entity, data: data)
    }
}

let database = Database()
