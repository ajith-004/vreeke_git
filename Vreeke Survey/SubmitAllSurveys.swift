//
//  SubmitAllSurveys.swift
//  Vreeke Survey
//
//  Created by Alokin on 01/02/19.
//  Copyright © 2019 Alokin. All rights reserved.
//

import UIKit

class SubmitAllSurveys: BaseViewController, DownloadDelegate {
    @IBOutlet weak var progressBar: UIProgressView!
    var progressBarTimer: Timer!
    // label and 2 buttons
    @IBOutlet weak var startSurvey: UIButton!
    @IBOutlet weak var manageSurveys: UIButton!
    @IBOutlet weak var surveyStatus: UILabel!
    @IBOutlet weak var pendingSurveys: UILabel!
    @IBOutlet weak var versionTag: UILabel!
    @IBOutlet weak var separatorView: UIView!

    let SURVEY_SUCCESS = "Your surveys have been sent successfully!"
    let SURVEY_FAILURE = "Your surveys were not submitted"

    var submittedSurveyCount: Int = 0
    var surveyCount: Int!
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .submitAllResponse, object: nil)
        NotificationCenter.default.removeObserver(self, name: .submitAllResponseFailed, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(submitSuccess), name: .submitAllResponse, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showSurveyButtonsWithWarning), name: .submitAllResponseFailed, object: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        logdata(#file, #function)
//        NotificationCenter.default.addObserver(self, selector: #selector(submitSuccess), name: .submitAllResponse, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(showSurveyButtonsWithWarning), name: .submitAllResponseFailed, object: nil)
        surveyCount = appData.completedSurveys?.count

//        progressBarTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { timer in
//            if self.progressBar.progress == 1.0 {
//                timer.invalidate()
//                self.showSurveyButtonsWithWarning()
//            } else {
//                self.progressBar.setProgress(self.progressBar.progress + 0.1, animated: true)
//            }
//        })

        /*
         upload images from survery response object
         update response object with image ids
         */
        versionTag.text = VERSION_TAG
        startSurvey.setRoundedCorners()
        manageSurveys.setRoundedCorners()
        if connectedToNetwork() {
            logdata(appData.surveyResponse)
            processSurveyResponse()
        } else {
            // show alert
            self.showSurveyButtonsWithWarning()
        }
    }

    func processSurveyResponse() {
        appData.setDBResponse(appData.completedSurveys![submittedSurveyCount])
        let psq = ProcessSurveyQuestions(questions: (appData.surveyResponse?.survey_response)!)
        psq.delegate = self
        psq.processQuestion()
    }

    @objc func showSurveyButtonsWithWarning() {
        DispatchQueue.main.async {
            self.progressBar.setProgress(1.0, animated: true)
            self.surveyStatus.text = self.SURVEY_FAILURE
            let alert = UIAlertController(title: "Warning", message: "The surveys could not be submitted. Please retry when you are online", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
            self.present(alert, animated: true, completion: {
                self.showSurveyButtons()
            })
        }
    }

    func showSurveyButtons() {
        startSurvey.isHidden = false
        manageSurveys.isHidden = false
        let pendingSurveyCount = getPendingSurveyCount()
        if pendingSurveyCount > 0 {
            pendingSurveys.text = getPendingSurveyText(pendingSurveyCount)
        }
        pendingSurveys.isHidden = false
        separatorView.isHidden = false
    }

    @objc func submitSuccess() {
        logdata(#file, #function, appData.dbResponse)
//        logdata(#function, database.updateData(of: "Response", with: "date=%@", arguments: [appData.dbResponse?.date], data: ["submitted": true]))
        if database.deleteData(from: "Response", with: "date=%@", arguments: [appData.dbResponse!.date]) {
//            let urlList = appData.dbResponse?.response.getImageURLs()
//            for urls in urlList! {
//                for url in urls {
//                    if url.contains("userUploads") {
//                        deleteFileAtPath(path: url)
//                    }
//                }
//            }
            appData.deleteUserUploadedImages()
            submittedSurveyCount += 1
            if submittedSurveyCount < surveyCount {
                DispatchQueue.main.async {
                    self.progressBar.setProgress(Float(self.submittedSurveyCount / self.surveyCount), animated: true)
                    //            self.surveyStatus.text = self.SURVEY_SUCCESS
                    //            self.showSurveyButtons()
                    appData.clearDBResponse()
                    self.processSurveyResponse()
                }
            } else {
                DispatchQueue.main.async {
                    self.progressBar.setProgress(1.0, animated: true)
                    self.surveyStatus.text = self.SURVEY_SUCCESS
                    self.showSurveyButtons()
                }
            }
        } else {
            // show warning??
        }
    }

    func didComplete(_ data: Any?) {
        logdata(#file, #function, data)
        logdata("process questions complteted")
        DispatchQueue.main.async {
            self.progressBar.setProgress(Float(self.submittedSurveyCount / (self.surveyCount * 2)), animated: true)
        }
        appData.surveyResponse?.survey_response = data as! [SurveyQuestion]
        do {
            let jsonData = try encoder.encode(appData.surveyResponse)
            request.submitResponse(data: jsonData, submitAll: true)
            logdata("submitSuccess", appData.dbResponse)
        } catch let err {
            logdata(err)
        }
    }
}
